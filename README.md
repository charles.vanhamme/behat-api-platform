# How to set up project ?

docker-compose up -d

docker-compose exec php bin/console d:s:u --force

docker-compose exec php bin/console hautelook:fixtures:load

docker-compose exec php vendor/bin/behat

\\# Scenarios

#### Posts

-   En tant que User connecté, je peux créer un Post.
-   En tant que User connecté, je peux modifier ce Post.
-   En tant que User connecté, je peux supprimer ce Post.
-   En tant que User connecté, je ne peux pas modifier un post dont je ne suis pas l'auteur.
-   En tant que User connecté, je ne peux pas supprimer un post dont je ne suis pas l'auteur.

#### Likes

-   En tant que User connecté, je peux ajouter un Likes sur un Post.
-   En tant que User connecté, je peux retirer mon Likes sur un Post.
-   En tant que User connecté, je ne peux pas retirer le Likes d'un autre User sur un Post.

#### Comments

-   En tant que User connecté, je peux ajouter un Comments sur un Post.
-   En tant que User connecté, je peux retirer mon Comments sur un Post.
-   En tant que User connecté, je peux retirer le Comments d'un autre User sur un Post dont je suis l'auteur.

#### Reports

-   En tant que User connecté, je peux ajouter un Reports sur un commentaire dont je ne suis pas l'auteur.
-   En tant que User connecté, je peux retirer mon Reports sur un commentaire dont je ne suis pas l'auteur.
-   En tant que User connecté, je ne peux pas ajouter un Reports sur un commentaire dont je suis l'auteur.
-   En tant que User connecté, je ne peux pas retirer le Reports d'un autre User sur un Commentaire.

#### Unauthenticated

-   L'ensemble des users stories ci-dessus ne sont pas disponibles en tant que Users non connecté.
