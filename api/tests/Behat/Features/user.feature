# This file contains a user story for demonstration only.
# Learn how to get started with Behat and BDD on Behat's website:
# http://behat.org/en/latest/quick_start.html

Feature: User Feature
    Scenario: Get list of users
        Given I set payload
        """
        {
          "email": "amorin@vetixy.com",
          "password": "amorin"
        }
        """
        Given I log in
        When I request to "GET" "/users"
        Then The response status code should be 200
        And The "content-type" header response should exist
        And The "content-type" header response should be "application/ld+json; charset=utf-8"

    Scenario: Get list of users
        Given I set payload
        """
        {
          "email": "amorin@vetixy.com",
          "password": "amorin"
        }
        """
        Given I log in
        When I request to "GET" "/users/XXX"
        Then The response status code should be 404

    Scenario: Post new user
        Given I set payload
        """
        {
          "email": "amorin@vetixy.com",
          "password": "amorin"
        }
        """
        Given I log in
        When I set payload
         """
        {
            "email": "test@test.fr",
            "password": "pwd"
        }
        """
        When I request to "POST" "/users"
        Then The response status code should be 201
        #And I add a reference "user"

        #When I request to "GET" "{user.@id}"
