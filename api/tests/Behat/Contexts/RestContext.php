<?php

declare(strict_types=1);

namespace App\Tests\Behat\Contexts;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Response;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Fidry\AliceDataFixtures\Loader\PersisterLoader;
use Hautelook\AliceBundle\PhpUnit\BaseDatabaseTrait;
use Symfony\Component\HttpKernel\KernelInterface;

final class RestContext extends ApiTestCase implements Context
{
    use HeaderContextTrait;
    use FixturesContextTrait;

    use BaseDatabaseTrait;
    use HookContextTrait;

    /** @var Response|null */
    private $lastResponse;

    /** @var PyStringNode */
    private $lastPayload;

    /** @var PersisterLoader */
    private $fixturesLoader;

    /** @var string */
    private $token;

    private static $getKernel;

    public function __construct(KernelInterface $kernel)
    {
        parent::__construct();
        self::$getKernel = $kernel;
        $this->fixturesLoader = $kernel->getContainer()->get('fidry_alice_data_fixtures.loader.doctrine');
    }

    /**
     * @When I request to :method :path
     */
    public function iSendARequestTo(string $method, string $path): void
    {
        $options = ['headers' => $this->headers];
        if ($this->token) {
            $options['headers']['Authorization'] = $this->token;
        }

        if ($this->lastPayload) {
            $options['body'] = $this->lastPayload->getRaw();
        }

        var_dump($options);

        $this->lastResponse = $this->createClient()->request($method, $path, $options);
    }

    /**
     * @Given I log in
     */
    public function iLogIn(): void
    {
        $this->iSendARequestTo('POST', '/authentication_token');

        $this->token = 'Bearer ' . json_decode($this->lastResponse->getContent())->token;

        var_dump($this->token);
    }

    /**
     * PAS UTILE SI PAYLOAD
     * @When I request to :method :path with data
     */
    public function iSendARequestWithData(string $method, string $path, PyStringNode $parameters): void
    {
        $this->lastResponse = $this->createClient()->request($method, $path, [
            'headers' => [
                'content-type' => 'application/ld+json'
            ],
            'body' => $parameters->getRaw()
        ]);
    }

    /**
     * @When I set payload
     */
    public function iSetPayload(PyStringNode $payload): void
    {
        $this->lastPayload = $payload;
    }

    /**
     * @param $statusCode
     * @Then The response status code should be :statusCode
     */
    public function theResponseStatusCodeShouldBe($statusCode)
    {
        if ($this->lastResponse->getStatusCode() != $statusCode) {
            //throw new \RuntimeException('Status code error');
            var_dump($this->lastResponse->getStatusCode());
        }
    }
}
